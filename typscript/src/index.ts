//test
console.log("coucou");

// fonction simple addition de 2 nombres
function add(a: number, b: number): number {
  let result: number = a + b;
  return result;
}
console.log(add(5, 6));

//taux de conversion
let euroCad: number = 1.5;
let euroJpy: number = 130;
let cadJpy: number = 87;

// conversion
function convert(deviseEntree: string, deviseSortie: string, montant: number) {
  if (deviseEntree === "CAD" && deviseSortie === "EUR") {
    return montant * (1 / euroCad) + "EUR";
  } else if (deviseEntree === "EUR" && deviseSortie === "CAD") {
    return montant * euroCad + "CAD";
  } else if (deviseEntree === "EUR" && deviseSortie === "JPY") {
    return montant * euroJpy + "JPY";
  } else if (deviseEntree === "JPY" && deviseSortie === "EUR") {
    return montant * (1 / euroJpy) + "EUR";
  } else if (deviseEntree === "JPY" && deviseSortie === "CAD") {
    return montant * (1 / cadJpy) + "CAD";
  } else if (deviseEntree === "CAD" && deviseSortie === "JPY") {
    return montant * cadJpy + "JPY";
  } else {
    console.error("Conversion de devises non prise en charge");
    return undefined;
  }
}
console.log(convert("EUR", "CAD", 100)); // 150 EUR
console.log(convert("EUR", "JPY", 50)); //6500 JPY

// frais de livraison
function calculator(poids: number, destination: string) {
  if (poids <= 1 && destination === "France") {
    return "le prix de la livraison est 10 EUR";
  } else if (poids <= 1 && destination === "Canada") {
    return "le prix de la livraison est 15 CAD";
  } else if (poids <= 1 && destination === "Japon") {
    return "le prix de la livraison est 1000 JPY";
  } else if (poids > 1 && poids <= 3 && destination === "France") {
    return "le prix de la livraison est 20 EUR";
  } else if (poids > 1 &&poids <= 3 && destination === "Canada") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids > 1 && poids <= 3 && destination === "Japon") {
    return "le prix de la livraison est 2000 JPY";
  } else if (poids>3 && destination === "France") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids>3 && destination === "Canada") {
    return "le prix de la livraison est 45 CAD";
  } else if (poids>3 && destination === "Japon") {
    return "le prix de la livraison est 3000 JPY";
  } else {
    console.error("Frais de port non définis");
    return undefined;
  }
}

//todo Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.

console.log(calculator(1, "France")); //10 euros
console.log(calculator(5, "Japon")); // 3000 JPY

// frais de douanes
function douaneTaxes(objectValue: number, country: string) {
  if (country === "france") {
    return 'pas de frais de douane en France'
  } else
    if (objectValue >= 15 && country === "Canada") {
      let taxeFees = objectValue * 0.15;
    return `les frais de douanes sont de ${taxeFees} CAD supplémentaires.`;
    } else if (objectValue >= 5000 && country === "Japon") {
      let taxeFees = objectValue * 0.10;
    return `les frais de douanes sont de ${taxeFees} JPY supplémentaires.`
  } else {
    console.error("Pays ou valeur non pris en charge pour les frais de douane.");
    return undefined;
  }
}

console.log(douaneTaxes(22, 'france'));
console.log(douaneTaxes(5600, "Japon"));